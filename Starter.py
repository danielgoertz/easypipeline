'''
Created on 23.12.2012

@author: danielgoertz

This class provide the entry point
of the whole program. It is holds
also the GUI elements and basic 
user interaction.
'''

import sys, os, logging
from PyQt4 import QtGui, QtCore

from Viewer.GUI import Ui_Form as View
from Viewer.Node import Node as Node
from Viewer.Edge import Edge as Edge
from Viewer.NodePopup import NodePopup as NodePopup

from Controler import Controler_Setup_Tab
from Controler import Controler_Version_Control_Tab
from Controler import Controler_File_Structure_Tab
import Helpers.Multi_Platform as Multi_platform

app = QtGui.QApplication(sys.argv)

class TestSortFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    This inner class provides only the functionality
    to  display objects into a treeview instance.     
    """
    def __init__(self, parent=None):
        super(TestSortFilterProxyModel, self).__init__(parent)
        self.filter = ['', ''];
    
    def filterAcceptsRow(self, source_row, source_parent):
        """
        Custom filter to display the content from the file system
        into a treeview.
        
        @param      source_row: 
        @type       QSortFilterProxyModel: Inherit object
        @return:
        """
        index0 = self.sourceModel().index(source_row, 0, source_parent)
        filePath = self.sourceModel().filePath(index0) 

        for folder in self.filter:
            if filePath.startsWith(folder) or QtCore.QString(folder).startsWith(filePath):
                return True;        
        return False

class Starter(QtGui.QDialog, View):
    """
    This class is the entry point of the whole applications.
    The GUI was created with the QT Designer and transformed with
    the command pyuic4.
    """

    def __init__(self):
        # Setup for Logging
        logging.basicConfig(format='%(levelname)s:%(message)s', filename = "Logfile.log", level= logging.DEBUG, filemode = "a")
        logging.info("START LOGGING....")
        
        QtGui.QDialog.__init__(self)
        self.setupUi(self)
        
        self.model = QtGui.QFileSystemModel(self)
        self.model.setRootPath(r"/Users/danielgoertz/")
        
        self.view = self.graphicsView_node_view
        self.scene = QtGui.QGraphicsScene()
        self.__scale_factor = 0.0

        self.scene.setSceneRect(165, 70, 741, 491) # If you change the size of the QGraphicsScene you must adjust these values too.
        self.view.setScene(self.scene)        
        
        self.platform = Multi_platform.get_current_platform()
        self.controler_setup_tab = Controler_Setup_Tab.Controler_Setup_Tab()
        self.controler_file_structure_tab = Controler_File_Structure_Tab.Controler_File_Structure_Tab()
        self.version_control_tab = Controler_Version_Control_Tab.Controler_Version_Control_Tab()
        
        self.startNode = None
        self.startEdgePos = None

        self.endNode = None
        self.endEdgePos = None
        
        #Customized slots to catch the singles from the GUI interactions
        # Setup Tab - Section
        self.connect(self.pushButton_auto_config, QtCore.SIGNAL("clicked()"), self.auto_config)
        self.connect(self.pushButton_browse_maya, QtCore.SIGNAL("clicked()"), self.file_browser_maya)
        self.connect(self.pushButton_browse_3dsmax, QtCore.SIGNAL("clicked()"), self.file_browser_3dsmax)
        self.connect(self.pushButton_browse_cinema4d, QtCore.SIGNAL("clicked()"), self.file_browser_cinema4d)
        self.connect(self.pushButton_browse_houdini, QtCore.SIGNAL("clicked()"), self.file_browser_houdini)
        self.connect(self.pushButton_browse_photoshop, QtCore.SIGNAL("clicked()"), self.file_browser_photoshop)
        self.connect(self.pushButton_browse_nuke, QtCore.SIGNAL("clicked()"), self.file_browser_nuke)
        self.connect(self.pushButton_browse_aftereffects, QtCore.SIGNAL("clicked()"), self.file_browser_after_effects)       
        self.connect(self.pushButton_install_plugins, QtCore.SIGNAL("clicked()"), self.install_plugins)
        self.connect(self.pushButton_remove_plugins, QtCore.SIGNAL("clicked()"), self.remove_plugins)        
        self.connect(self.pushButton_close_program, QtCore.SIGNAL("clicked()"), self.close_programm)
        self.connect(self.pushButton_run_setup, QtCore.SIGNAL("clicked()"), self.run_setup)
        self.connect(self.pushButton_save_settings, QtCore.SIGNAL("clicked()"), self.save_setting)


        # File Structure - Section         
        self.connect(self.listWidget_node_list, QtCore.SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.item_clicked)
        self.connect(self.pushButton_create_node, QtCore.SIGNAL("clicked()"), self.openNodePopup)
        self.connect(self.pushButton_delete_node, QtCore.SIGNAL("clicked()"), self.deleteNodeFromList)
        self.connect(self.pushButton_clean_layout, QtCore.SIGNAL("clicked()"), self.clean_qGraphicsScene)
        self.connect(self.pushButton_zoom_in, QtCore.SIGNAL("clicked()"), self.zoom_in)
        self.connect(self.pushButton_zoom_out, QtCore.SIGNAL("clicked()"), self.zoom_out)
        self.connect(self.pushButton_zoom_edit, QtCore.SIGNAL("clicked()"), self.normalize_view)
        self.connect(self.pushButton_analyze_layout, QtCore.SIGNAL("clicked()"), self.analyze_layout)
        self.connect(self.pushButton_save_layout, QtCore.SIGNAL("clicked()"), self.save_node_graph)
        self.connect(self.pushButton_load_layout, QtCore.SIGNAL("clicked()"), self.load_node_graph)
        self.connect(self.pushButton_create_data_structure, QtCore.SIGNAL("clicked()"), self.create_data_structure)

        # Create Node-List
        lv = self.listWidget_node_list
        lv.setViewMode(QtGui.QListView.IconMode)
        lv.setLayoutMode(QtGui.QListView.SinglePass)
        lv.setDragEnabled(False)
        lv.setDragDropOverwriteMode(False)
        lv.setDragDropMode(QtGui.QAbstractItemView.NoDragDrop)
        
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r"/Users/danielgoertz/Dropbox/workspace/AssetManager/Doc/Img/Setup_Screen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        
        # Version Control - Section
        proxy = TestSortFilterProxyModel(self)
        proxy.setSourceModel(self.model)

        self.treeView_file_explorer.setModel(proxy)
        self.treeView_file_explorer.connect(self.treeView_file_explorer, QtCore.SIGNAL('clicked(QModelIndex)'), self.update_previewer)

        # Load last settings
        self.load_settings()
        
   
    def item_clicked(self, current_list_item):
        """
        Double click event for the Node List in the section 'File Structure'.
        It creates a Node depend of the data set which comes with the QListWidgetItem.
        
        @param    current_list_item: List of all created Nodes
        @type     current_list_item: QListWidgetItem
        """
        #Add Node-Item to the View
        node_path = current_list_item.data(QtCore.Qt.UserRole).toPyObject()[0]
        node_name = current_list_item.data(QtCore.Qt.UserRole).toPyObject()[1]
        node_color = current_list_item.data(QtCore.Qt.UserRole).toPyObject()[2]
        node_root = current_list_item.data(QtCore.Qt.UserRole).toPyObject()[3]
        node = Node(node_path, node_name, node_color)
        if node_root == "True":
            node.set_as_root()
        self.scene.addItem(node)
        node.setPos(400, 400)
        
    def get_scale_factor(self):
        """
        Getter method to get the current
        scale factor.
        
        @rtype:   int 
        @return:  scale factor
        """
        return self.__scale_factor
    
    def set_scale_factor(self, new_scale_factor):
        """
        Setter method to set the current
        scale factor.
        
        @param    new_scale_factor: new scale factor
        @type     new_scale_factor: int
        """
        
        self.__scale_factor = new_scale_factor
        
        
    """    
    def wheelEvent(self, event):
        self.set_scale_factor = math.pow(2.0, -event.delta() / 240.0)
        self.controler_file_structure_tab.scale_view(self.view, self.get_scale_factor())
    """ 
             
    def mousePressEvent(self, event):
        """
        Overwritten event method to customise 
        the click behaviour into the QGraphicsScene.
        In this method handled how a Node can be moved,
        connect to an other Node as well as disconnect. 
        
        @param   event: MousePressEvent
        @type    event: MousePressEvent
        """      
        
        mouse_pos = self.view.mapFromScene(event.x(),event.y())
        #print mouse_pos
               
        if self.startNode == None:
            for node in self.scene.items():
                node_pos = self.view.mapFromScene(node.pos().x(), node.pos().y())
                #print nodePos       
                
                if ((mouse_pos.x() <= node_pos.x()) and (mouse_pos.x() >= (node_pos.x()-30))) and ( (mouse_pos.y() <= (node_pos.y()+15)) and (mouse_pos.y() >= (node_pos.y()-10))): 
                    self.startNode = node
                    self.startEdgePos = mouse_pos
                    #print "Source-Node Input"
                    
                elif ((mouse_pos.x() >= node_pos.x()) and (mouse_pos.x() <= (node_pos.x()+50))) and ( (mouse_pos.y() <= (node_pos.y()+15)) and (mouse_pos.y() >= (node_pos.y()-10))):
                    self.startNode = node
                    self.startEdgePos = mouse_pos
                    #print "Source-Node Output"
        else:
            for item in self.scene.items():
                if type(item) == Node:
                    destNode = item
                    node_pos = self.view.mapFromScene(item.pos().x(), item.pos().y())
                    
                    if destNode != self.startNode:
                        if ((mouse_pos.x() <= node_pos.x()) and (mouse_pos.x() >= (node_pos.x()-30))) and ( (mouse_pos.y() <= (node_pos.y()+15)) and (mouse_pos.y() >= (node_pos.y()-10))):
                            if self.startEdgePos != None:
                                #print item.get_busy_input()
                                if (not self.has_edge(self.startNode, destNode)) and (item.get_busy_input() == False):
                                    self.scene.addItem(Edge(self.startNode, self.startEdgePos, destNode, mouse_pos))                            
                                    self.startEdgePos = None
                                    self.startNode = None
                                    self.endNode = None
                                    item.set_busy_input()
                                else:
                                    QtGui.QMessageBox.information(self, 'Info', "Multiple Input-Connections are not permitted!")
                            #print "Destination-Node Input"
                            
                        elif ((mouse_pos.x() >= node_pos.x()) and (mouse_pos.x() <= (node_pos.x()+50))) and ( (mouse_pos.y() <= (node_pos.y()+15)) and (mouse_pos.y() >= (node_pos.y()-10))):
                            if not self.has_edge(self.startNode, destNode):
                                self.scene.addItem(Edge(self.startNode, self.startEdgePos, destNode, mouse_pos))
                                self.startEdgePos = None
                                self.startNode = None
                                self.endNode = None                                 
                            #print "Destination-Node Output"

                        
    def has_edge(self, sourceNode, destNode):
        """
        Check if exists a Edge between two Nodes.
        
        @param    sourceNode: Source Node
        @type     sourceNode: Node
        @param    destNode:   Destination Node
        @type     destNode:   Node
        @rtype:               Boolean
        @return:              Exists an Edge already or not        
        """
        for item in self.scene.items():
            if type(item) == Node:                 
                for edge in item.edges():
                    if (edge.destNode() == sourceNode or edge.destNode() == destNode) and (edge.sourceNode() == sourceNode or edge.sourceNode() == destNode):                                            
                        self.controler_file_structure_tab.remove_edge(self.scene, edge)
                        item.edges().remove(edge)
                        self.startEdgePos = None
                        self.startNode = None
                        self.endNode = None 
                        return True
                    else:
                        return False

    def auto_config(self):
        """
        Comparable as a manager method who
        get the necessarily data to set them into
        the set_program_paths method.
        
        @param     None:None
        @return:        None  
        """
        dict_paths = self.controler_setup_tab.auto_config()
        self.set_program_paths(dict_paths)
   
    def set_program_paths(self, dict_paths):
        """
        Method to fill the GUI elements 'LineEdit'
        with the right paths to the executable program.
        
        @param dict_paths: Structure is "Name of program" : "Path to executable"
        @type  dict_paths: Simple Dictionary      
        """
        for key in dict_paths:
            if key == "Maya" and dict_paths[key] != "":
                self.lineEdit_maya_exe.setText(dict_paths[key])
            if key == "3dsmax" and dict_paths[key] != "":
                self.lineEdit_3dsmax_exe.setText(dict_paths[key])
            if key == "Cinema4d" and dict_paths[key] != "":
                self.lineEdit_cinema4d_exe.setText(dict_paths[key])                                  
            if key == "Houdini" and dict_paths[key] != "":
                self.lineEdit_houdini_exe.setText(dict_paths[key])   
            if key == "Photoshop" and dict_paths[key] != "":
                self.lineEdit_photoshop_exe.setText(dict_paths[key])   
            if key == "Nuke" and dict_paths[key] != "":
                self.lineEdit_nuke_exe.setText(dict_paths[key])
            if key == "After_effects" and dict_paths[key] != "":
                self.lineEdit_aftereffects_exe.setText(dict_paths[key])
    
    def file_browser_maya(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/Autodesk', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\Autodesk', '*.exe')            
        self.lineEdit_maya_exe.setText(filename)
        
    def file_browser_3dsmax(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """
        filename = ""
        # 3DS Max is not available for macosx!
        if self.platform == "macosx":
            self.lineEdit_3dsmax_exe.setText("3DS Max is not avaiable for Macintosh")
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\Autodesk', '*.exe')            
        self.lineEdit_3dsmax_exe.setText(filename)

    def file_browser_cinema4d(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """        
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\', '*.exe')            
        self.lineEdit_cinema4d_exe.setText(filename)

    def file_browser_houdini(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """        
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\Side Effects Software', '*.exe')
        self.lineEdit_houdini_exe.setText(filename)
        
    def file_browser_photoshop(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """        
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\Adobe', '*.exe')            
        self.lineEdit_photoshop_exe.setText(filename)
        
    def file_browser_nuke(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """        
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\', '*.exe')            
        self.lineEdit_nuke_exe.setText(filename)
        
    def file_browser_after_effects(self):
        """
        Simple method to open a File Dialog to select manually the path the program executable
        """        
        filename = ""
        if self.platform == "macosx":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/Applications/', '*.app')
        if self.platform == "windows":
            filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Program Files\\Adobe', '*.exe')            
        self.lineEdit_aftereffects_exe.setText(filename)
        
    def install_plugins(self):
        """
        """
        
        # Create a default dict for the incoming results
        dict_plugins = {"GeoCache": False, "AnimCamera":False, "QualityCheck":False, "FurRIB": False, "BatchMode":False}
        
        if self.checkBox_imex_geocache.isChecked():
            dict_plugins["GeoCache"] = True
        if self.checkBox_imex_anim_cam.isChecked():
            dict_plugins["AnimCamera"] = True
        if self.checkBox_quality_check.isChecked():
            dict_plugins["QualityCheck"] = True
        if self.checkBox_ex_fur_rib.isChecked():
            dict_plugins["FurRIB"] = True
        if self.checkBox_batch_mode_app.isChecked():
            dict_plugins["BatchMode"] = True
            
        maya_path = self.lineEdit_maya_exe.text()
        if maya_path != "":
            self.controler_setup_tab.install_plugins(dict_plugins)
        else:
            QtGui.QMessageBox.critical(self, 'Info Message', ''' Autodesk Maya installation not found!''',
            QtGui.QMessageBox.Ok)
            
    def remove_plugins(self):
        self.controler_setup_tab.remove_plugins()
            
    def update_previewer(self, index):
        item = index.data().toString()
        # Der Pfad zum Bild kommt aus der DB nicht vom FileSystem!
        qPixmap = QtGui.QPixmap(os.getcwd() + "/Doc/Img/Setup_Screen.png")
        #        
        self.image_preview.setPixmap(qPixmap)
        self.image_preview.setScaledContents(True)
        print (os.getcwd() + "Doc/Img/Setup_Screen.png")
        print item
        
    def run_setup(self):
        status = self.controler_setup_tab.run_setup()
        self.label_setup_status.setText(status)
        
    def display_node_graph(self, node_graph_list):
        # Let's create the nodes first
        for node in node_graph_list:
            
            node_attributes = node[0]                        
            
            q_color = QtGui.QColor(0, 0, 0)
            q_color.setNamedColor(node_attributes["color"])            
            
            new_node = Node(node_attributes["path"], node_attributes["name"], q_color)
            new_node.set_busy_input()
            if node_attributes["root"] == "True":
                new_node.set_as_root()
                            
            self.scene.addItem(new_node)            
            pos = node_attributes["pos"]
            pos  = pos.split(",")     
            new_node.setPos(QtCore.QPointF(float(pos[0]), float(pos[1])))
            
            logging.log(logging.INFO, "Restored Node in Graph : " + node_attributes["name"] +", "+ node_attributes["path"] + ", " + node_attributes["root"] + ", " + node_attributes["color"])
            
        # And now the Edges for each Node
        for node in node_graph_list:
            for start_item in self.scene.items():
                if type(start_item) == Node:
                    #let's find the start_node
                    if node[0]["name"] == start_item.getName():
                        #now we need the dest_node
                        for end_item in self.scene.items():
                            if type(end_item) == Node:
                                
                                """
                                for conn in node[1]["input"]: 
                                    if conn["destNode"] == end_item.getName():
                                        self.scene.addItem(Edge(start_item, conn["sourcePoint"], end_item, conn["destPoint"]))
                                """
                                for conn in node[1]["output"]: 
                                    if conn["destNode"] == end_item.getName():
                                        self.scene.addItem(Edge(start_item, conn["sourcePoint"], end_item, conn["destPoint"]))
    
    def load_settings(self):
        dict_last_settings = self.controler_setup_tab.load_settings()[0]
        node_list = self.controler_setup_tab.load_settings()[1]
        dict_last_plugins = self.controler_setup_tab.load_settings()[2]
        node_graph_list = self.controler_setup_tab.load_settings()[3]
        
        # Display the Node-Graph                    
        self.display_node_graph(node_graph_list)

        # Load the Paths to the Executables  
        if dict_last_settings != None:
            for key in dict_last_settings:
                if key == "Maya" and dict_last_settings[key] != None:
                    self.lineEdit_maya_exe.setText(dict_last_settings[key])
                elif key == "3DSMax" and dict_last_settings[key] != None:
                    self.lineEdit_3dsmax_exe.setText(dict_last_settings[key])
                elif key == "Houdini" and dict_last_settings[key] != None:
                    self.lineEdit_houdini_exe.setText(dict_last_settings[key])
                elif key == "AfterEffects" and dict_last_settings[key] != None:
                    self.lineEdit_aftereffects_exe.setText(dict_last_settings[key])
                elif key == "Cinema4D" and dict_last_settings[key] != None:
                    self.lineEdit_cinema4d_exe.setText(dict_last_settings[key])
                elif key == "Nuke" and dict_last_settings[key] != None:
                    self.lineEdit_nuke_exe.setText(dict_last_settings[key])
                elif key == "Photoshop" and dict_last_settings[key] != None:
                    self.lineEdit_photoshop_exe.setText(dict_last_settings[key])
                    
        for i in xrange(len(node_list)):
            node_path = node_list[i][1] #Path
            node_name = node_list[i][3] #Name
            node_color = node_list[i][5] #Color
            node_root = node_list[i][7] #Color
            
            q_color = QtGui.QColor(0, 0, 0)
            q_color.setNamedColor(node_color)
            
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r"/Users/danielgoertz/Dropbox/workspace/AssetManager/Doc/Img/Setup_Screen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            item = QtGui.QListWidgetItem(node_path)
            item.setIcon(icon)
            item.setData(QtCore.Qt.UserRole, (node_name,node_path, q_color, node_root))
            self.listWidget_node_list.addItem(item)
            logging.log(logging.INFO, "Load Node from Node List: " + node_name +", "+ node_path + ", " + node_color + ", " + node_root)
            
        for key in dict_last_plugins:
            if key == "GeoCache":
                if dict_last_plugins[key] == "True":
                    self.checkBox_imex_geocache.setChecked(1)
                else:
                    self.checkBox_imex_geocache.setChecked(0)
            elif key == "Camera":
                if (dict_last_plugins[key]) == "True":
                    self.checkBox_imex_anim_cam.setChecked(1)
                else:
                    self.checkBox_imex_anim_cam.setChecked(0)
            elif key == "QC":
                if (dict_last_plugins[key]) == "True":
                    self.checkBox_quality_check.setChecked(1)
                else:
                    self.checkBox_quality_check.setChecked(0)
            elif key == "FurRIB":
                if (dict_last_plugins[key]) == "True":
                    self.checkBox_ex_fur_rib.setChecked(1)
                else:
                    self.checkBox_ex_fur_rib.setChecked(0)
            elif key == "Batch":
                if dict_last_plugins[key] == "True":
                    self.checkBox_batch_mode_app.setChecked(1)
                else:
                    self.checkBox_batch_mode_app.setChecked(0)

    def save_setting(self):
        dict_launcher_paths = {"Maya": str(self.lineEdit_maya_exe.text()),
                               "3DSMax": str(self.lineEdit_3dsmax_exe.text()),
                               "Houdini": str(self.lineEdit_houdini_exe.text()),
                               "AfterEffects": str(self.lineEdit_aftereffects_exe.text()),
                               "Cinema4D": str(self.lineEdit_cinema4d_exe.text()),
                               "Nuke": str(self.lineEdit_nuke_exe.text()),
                               "Photoshop": str(self.lineEdit_photoshop_exe.text())
                               }
        
        dict_maya_plugins = {"GeoCache": str(self.checkBox_imex_geocache.isChecked()),
                             "Camera": str(self.checkBox_imex_anim_cam.isChecked()),
                             "QC": str(self.checkBox_quality_check.isChecked()),
                             "FurRIB": str(self.checkBox_ex_fur_rib.isChecked()),
                             "Batch": str(self.checkBox_batch_mode_app.isChecked())
                             }
        
        node_list = self.listWidget_node_list
        node_graph = self.scene 
        
        self.controler_setup_tab.save_setting(dict_launcher_paths, dict_maya_plugins, node_list, node_graph)

    def openNodePopup(self):    
        self.nodePopup = NodePopup(self.listWidget_node_list)
        self.nodePopup.setGeometry(QtCore.QRect(200, 200, 500, 200))
        self.nodePopup.show()
        
    def deleteNodeFromList(self):
        self.controler_file_structure_tab.remove_item(self.listWidget_node_list)
        
    def clean_qGraphicsScene(self):
        self.controler_file_structure_tab.clean_qGraphicsScene(self.scene)
        
    def zoom_in(self):
        if self.get_scale_factor() >= -2.0 and self.get_scale_factor() <= 2.0:
            self.set_scale_factor( self.get_scale_factor() + 2.0 )            
            self.controler_file_structure_tab.scale_view(self.view, 2.0)
        
    def zoom_out(self):  
        if self.get_scale_factor() >= -2.0 and self.get_scale_factor() <= 2.0:
            self.set_scale_factor( self.get_scale_factor() -2.0 )      
            self.controler_file_structure_tab.scale_view(self.view, 0.5)
        
    def normalize_view(self):
        if self.get_scale_factor() != 0.0:
            self.controler_file_structure_tab.normalize_view(self.view, self.get_scale_factor())
            self.set_scale_factor(0.0)
            
    def save_node_graph(self):
        format = "XML"      
        dialog = QtGui.QFileDialog(self, self.tr("Save As"))
        dialog.setAcceptMode(QtGui.QFileDialog.AcceptSave)
        dialog.setDefaultSuffix(format.lower())
        dialog.setConfirmOverwrite(True)
        dialog.setDirectory("/Users")
        dialog.setNameFilter('%s file (*.%s)' % (format, format.lower()))
        
        if dialog.exec_():
            xml_file_name = dialog.selectedFiles()[0]
            self.controler_file_structure_tab.save_node_graph(xml_file_name, self.scene)
            
    def load_node_graph(self):
        xml_file = QtGui.QFileDialog.getOpenFileName(self, self.tr("Open XML file"), "/home", self.tr("XML File (*.xml)"));        
        load_node_graph = self.controler_file_structure_tab.load_node_graph(xml_file)
        self.clean_qGraphicsScene()
        self.display_node_graph(load_node_graph)        


    def analyze_layout(self):
        status = self.controler_file_structure_tab.undirected_cycle_test(self.scene)
        QtGui.QMessageBox.information(self, 'Result', str(status))
        
    def create_data_structure(self):
        status = self.controler_file_structure_tab.create_data_structure(self.scene)
        #QtGui.QMessageBox.information(self, 'Result', str(status))

    def close_programm(self):        
        reply = QtGui.QMessageBox.question(self,'Message',"Are you sure to quit?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            logging.shutdown()
            sys.exit(app.exec_())
        else:
            pass
    
dialog = Starter() 
dialog.show() 
sys.exit(app.exec_())