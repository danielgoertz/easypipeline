'''
Created on 11.02.2013

@author: danielgoertz
'''

class User_Data(object):

    def __init__(self, name, role, position, settings, workstation, custom_node_list):
        self.name = name
        self.role = role
        self.position = position
        self.settings = settings
        self.workstation = workstation
        self.custom_node_list = custom_node_list
    
        self.user_data_dict = {"user_name": self.name, "role": self.role, "position": position, "settings": self.settings, "workstation": self.workstation, "custom_node_list": self.custom_node_list}
    
    def get_user_data_dict(self):
        return self.user_data_dict    

    def push_user_data(self):
        pass
        
    def pull_user_data(self):
        pass
    
    def __del__(self):
        pass