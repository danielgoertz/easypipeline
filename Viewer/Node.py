'''
Created on 14.01.2013

@author: danielgoertz
'''

from PyQt4 import QtGui, QtCore

class Node(QtGui.QGraphicsItem):
    Type = QtGui.QGraphicsItem.UserType + 1
    
    def __init__(self, node_path, node_name, node_color, parent = None):
        QtGui.QGraphicsItem.__init__(self,parent)
        
        self.__edgeList = []
        self._newPos = QtCore.QPointF()
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
        self.setCacheMode(QtGui.QGraphicsItem.DeviceCoordinateCache)
        self.__node_name = node_name
        self.__node_color = node_color
        self.__node_path = node_path
        self.__busy_input = False
        self.__root = "False"
        
    def boundingRect(self):
        adjust = 2.0
        return QtCore.QRectF(-30 - adjust, -30 - adjust, 83 + adjust, 48 + adjust)

    def shape(self):
        path = QtGui.QPainterPath()
        path.addRect(-10, -10, 40, 80)        
        return path
    
    def is_root(self):
        return self.__root
    
    def set_as_root(self):
        self.__root = "True"
    
    def set_busy_input(self):
        self.__busy_input = True
        
    def get_busy_input(self):
        return self.__busy_input
    
    def setPath(self, newPath):
        self.__node_path = newPath
        
    def getPath(self):
        return self.__node_path
    
    def setName(self, newName):
        self.__node_name = newName
    
    def getName(self):
        return self.__node_name
    
    def setColor(self, color):
        self.__node_color = color
    
    def getColor(self):
        return self.__node_color
    
  
    def type(self):
        return Node.Type

    def addEdge(self, edge):
        self.__edgeList.append(edge)
        edge.adjust()

    def edges(self):
        return self.__edgeList
    
    def get_pos(self):
        point = str(self.pos().x()) + "," + str(self.pos().y()) 
        return point
    
    def advance(self):
        if self._newPos == self.pos():            
            return False

        self.setPos(self._newPos)
        return True

    def mousePressEvent(self, event):
        self.update()
        for edge in self.__edgeList:
            if self == edge.source:
                edge.setSourceNode(self)
            else:
                edge.setDestNode(self)
            edge.update()
        super(Node, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        super(Node, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        self.update()
        for edge in self.__edgeList:
            if self == edge.source:
                edge.setSourceNode(self)
            else:
                edge.setDestNode(self)
            edge.update()
        super(Node, self).mouseMoveEvent(event)


    def paint(self, painter, option, widget):
         
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtCore.Qt.darkGray)
        painter.drawEllipse(-7, -7, 20, 20)       

        painter.setPen(QtGui.QPen(QtCore.Qt.black, 0))
        painter.drawEllipse(-10, -10, 20, 20)
        
        painter.setBrush(self.__node_color)
        painter.drawRect(-10, -10, 40, 25) # Middle Rect for the inner color of the node
        painter.setBrush(QtCore.Qt.gray)
        painter.drawRect(-30, -10, 20, 25) # Left Rect for the inputs
        painter.drawRect(30, -10, 20, 25) # Right Rect for the outputs
        
        painter.drawText(-30, -15,self.getPath())
        