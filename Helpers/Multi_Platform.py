'''
Created on 28.12.2012

@author: danielgoertz
'''

from distutils import util

def get_current_platform():
    os_type = util.get_platform()
    os_prefix = os_type.split("-")
    return os_prefix[0]
    
