'''
Created on 23.12.2012

@author: danielgoertz
'''

class AbstractControler_Setup(object):
    
    def get_path_maya(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_3dsmax(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_cinema4d(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_houdini(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_photoshop(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_nuke(self, default_path):
        raise NotImplementedError( "Should have implemented this" )
    
    def get_path_after_effects(self, default_path):
        raise NotImplementedError( "Should have implemented this" )