'''
Created on 23.12.2012

@author: danielgoertz
'''
import os, logging, shutil
from os import listdir
from os.path import expanduser

from AbstractControler_Setup import AbstractControler_Setup
from Helpers import Xml_Parser
from DBManager import DBHandler
import Helpers.Multi_Platform as multi_platform

class Controler_Setup_Tab(AbstractControler_Setup):
    
    def __init__(self):        
        self._Os_platform = multi_platform.get_current_platform()
                        
        self.xml_parser_version = Xml_Parser.Xml_Parser()
        self.settings = Xml_Parser.Xml_Parser()
                       
        if self._Os_platform == "macosx":
            self._default_paths = {"Maya":"/Applications/Autodesk/", "3DSMax":"", "Photoshop":"/Applications/", "Cinema4d": "", "Houdini":"/Applications/"}
            
            self.xml_parser_version.parse_xml_file('Resources/easyPipeline_supportVersion.xml')            
            self._appData_folder = expanduser("~")+"/Applications/EasyPipeline/Settings/"
            
            if os.path.isfile(self._appData_folder + "Settings.xml"):
                self._xml_settings_file = self._appData_folder + "Settings.xml"
                self.settings.parse_xml_file(self._xml_settings_file)
            else:
                pass
                
        elif self.__Os_platform == "win32":
            self._default_paths = {"Maya":"/Applications/Autodesk/", "3DSMax":"", "Photoshop":"/Applications/", "Cinema4d": "", "Houdini":"/Applications/"}
            self.xml_parser_version.parse_xml_file(r'C:\Users\Tokyo\Dropbox\workspace\AssetManager\Resources\easyPipeline_supportVersion.xml')
            self._appData_folder = expanduser("~")+"/AppData/Local/EasyPipeline/Settings/"
            
            if os.path.isfile(self._appData_folder + "Settings.xml"):
                self._xml_settings_file = self._appData_folder + "Settings.xml"
                self.settings.parse_xml_file(self._xml_settings_file)
            else:
                pass
        else:
            logging.log(logging.CRITICAL, "Not Supported OS yet, sorry.")

    def auto_config(self):
        paths_result = self.get_program_paths()
        return paths_result
    
    def get_platform(self):
        return self._Os_platform
            
    def get_program_paths(self):       
        result_paths = self.get_fullpath(self._default_paths)
        return result_paths       

    def get_path_maya(self, default_path):
        full_path = ""
        if not os.path.exists(default_path) and (not os.path.isfile(default_path)):            
            folders_list = listdir(default_path)
            for folder in folders_list:
                if folder.find("maya") != -1:
                    versions = self.xml_parser_version.get_support_version_by_name("Maya")
                    for version in versions:
                        if version in folder:
                            full_path = default_path + folder + "/" + "Maya.app"
                            return full_path
        else:
            return default_path

    
    def get_path_3dsmax(self, default_path):
        full_path = ""
        return full_path
    
    def get_path_cinema4d(self, default_path):
        full_path = ""
        return full_path
    
    def get_path_houdini(self, default_path):
        full_path = ""
        if (not os.path.exists(default_path) and (not os.path.isfile(default_path)) ):
            folders_list = listdir(default_path)
            for folder in folders_list:
                if folder.find("Houdini") != -1:
                    versions = self.xml_parser_version.get_support_version_by_name("Houdini")
                    for version in versions:
                        temp_split = folder.split(".")
                        for split in temp_split:
                            if split.find(version) != -1:
                                full_path = default_path + folder + "/" + "Houdini.app"
                                return full_path
        else:
            return default_path        
    
    def get_path_photoshop(self, default_path):
        full_path = ""
        if (not os.path.exists(default_path) and (not os.path.isfile(default_path)) ):
            folders_list = listdir(default_path)
            for folder in folders_list:
                if folder.find("Photoshop") != -1:
                    versions = self.xml_parser_version.get_support_version_by_name("Photoshop")
                    for version in versions:
                        if version in folder:
                            full_path = default_path + folder + "/" + "Adobe Photoshop CS6.app"
                            return full_path
        else:
            return default_path
    
    def get_path_nuke(self, default_path):
        full_path = ""
        return full_path
    
    def get_path_after_effects(self, default_path):
        full_path = ""
        return full_path
    
    def get_fullpath(self, dict_default_paths): 
        for key in dict_default_paths:
            if key == "Maya":
                dict_default_paths[key] = self.get_path_maya(dict_default_paths[key])
            if key == "3DSMax":
                dict_default_paths[key] = self.get_path_3dsmax(dict_default_paths[key])
            if key == "Cinema4d":
                dict_default_paths[key] = self.get_path_cinema4d(dict_default_paths[key])
            if key == "Houdini":
                dict_default_paths[key] = self.get_path_houdini(dict_default_paths[key])
            if key == "Photoshop":
                dict_default_paths[key] = self.get_path_photoshop(dict_default_paths[key])
            if key == "Nuke":
                dict_default_paths[key] = self.get_path_nuke(dict_default_paths[key])
            if key == "AfterEffects":
                dict_default_paths[key] = self.get_path_after_effects(dict_default_paths[key])           
        return dict_default_paths
    
    def install_plugins(self, dict_plugins):
        dest_maya_script_path = self.xml_parser_version.get_script_path_by_name("Maya", self._Os_platform)
        src_maya_scripts = ""
        src_maya_script_path = ""

        if self._Os_platform == "macosx":
            src_maya_script_path = "Resources/Maya_Scripts/"            
            src_maya_scripts = os.listdir(src_maya_script_path)
        else:
            src_maya_scripts = ""
            src_maya_scripts = os.listdir(src_maya_script_path)
                    
        if dict_plugins["GeoCache"] == True:
            for script in src_maya_scripts:
                if script.find("Geo") != -1:
                    shutil.copy(src_maya_script_path+script, dest_maya_script_path)
                    logging.log(logging.INFO, "Copied: Maya Plugin'" + script +"' to " +  dest_maya_script_path)
                    
        elif dict_plugins["AnimCamera"] == True:
            for script in src_maya_scripts:
                if script.find("Cam") != -1:
                    shutil.copy(src_maya_script_path+script, dest_maya_script_path)
                    logging.log(logging.INFO, "Copied: Maya Plugin'" + script +"' to " +  dest_maya_script_path)
                    
        elif dict_plugins["QualityCheck"] == True:
            for script in src_maya_scripts:
                if script.find("Quality") != -1:
                    shutil.copy(src_maya_script_path+script, dest_maya_script_path)
                    logging.log(logging.INFO, "Copied: Maya Plugin'" + script +"' to " +  dest_maya_script_path)                    

        elif dict_plugins["FurRIB"] == True:
            for script in src_maya_scripts:
                if script.find("Fur") != -1:
                    shutil.copy(src_maya_script_path+script, dest_maya_script_path)
                    logging.log(logging.INFO, "Copied: Maya Plugin'" + script +"' to " +  dest_maya_script_path)
                        
        elif dict_plugins["BatchMode"] == True:
            for script in src_maya_scripts:
                if script.find("Batch") != -1:
                    shutil.copy(src_maya_script_path+script, dest_maya_script_path)
                    logging.log(logging.INFO, "Copied: Maya Plugin'" + script +"' to " +  dest_maya_script_path)
        
    def run_setup(self):
        db_manager = DBHandler.DBHandler()
        
        xml_setting_file = "Settings.xml"
        xml_setting_path = "Resources/"
        status = db_manager.test_connection()
        if status == None:
            if self.get_platform() == "macosx":
                appData_folder = self._appData_folder
                if not os.path.exists(appData_folder):
                    os.makedirs(appData_folder)
                else:
                    shutil.copy(xml_setting_path + xml_setting_file, appData_folder)
                    self._xml_settings_file = self._appData_folder + "Settings.xml"
                    self.settings.parse_xml_file(self._xml_settings_file)
                    logging.info("")
                    return (xml_setting_path + xml_setting_file)
            else:
                appData_folder = self._appData_folder
                if not os.path.exists(appData_folder):
                    os.makedirs(appData_folder)
                else:
                    shutil.copy(xml_setting_path + xml_setting_file, appData_folder)
                    logging.info("")
                    return ("Copied: "+xml_setting_path + xml_setting_file)
        else:
            self.logging.log(self.logging.CRITICAL, status)
            return status
        
    def remove_plugins(self):
        dest_maya_script_path = self.xml_parser_version.get_script_path_by_name("Maya", self._Os_platform)
        src_maya_scripts = ""
        
        if self._Os_platform == "macosx":                 
            src_maya_scripts = os.listdir(dest_maya_script_path)
        else:
            src_maya_scripts = ""
            src_maya_scripts = os.listdir(dest_maya_script_path)
            
        if os.path.exists(dest_maya_script_path) and len(os.listdir(dest_maya_script_path)) > 0: 
            for script in src_maya_scripts:
                
                string_token = script.split(".")
                if len(string_token) >= 2:
                    string_token = string_token[0].split("_")
                    if len(string_token) >= 2:
                        string_token = string_token[1]
                  
                        if string_token.find("Geo") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin '" + script +"' from " +  dest_maya_script_path)                    
                        elif string_token.find("Cam") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin'" + script +"' from " +  dest_maya_script_path)
                        elif string_token.find("Light") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin '" + script +"' from " +  dest_maya_script_path)
                        elif string_token.find("Quality") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin '" + script +"' from " +  dest_maya_script_path)
                        elif string_token.find("FurRIB") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin '" + script +"' from " +  dest_maya_script_path)
                        elif string_token.find("Batch") != -1:
                            os.remove(dest_maya_script_path + script)
                            logging.log(logging.INFO, "Removed Maya Plugin '" + script +"' from " +  dest_maya_script_path)
    
    def save_setting(self, dict_launcher_paths, dict_maya_plugins, node_list, node_graph):        
        self._xml_settings_file = self._appData_folder + "Settings.xml" 
        self.settings.set_launcher_paths(self._xml_settings_file, dict_launcher_paths)
        self.settings.set_plugins(self._xml_settings_file, dict_maya_plugins)
        self.settings.set_node_list(self._xml_settings_file, node_list)
        self.settings.set_node_graph(self._xml_settings_file, node_graph)
        
    def load_settings(self):
        self._appData_folder + "Settings.xml"
        self._xml_settings_file = self._appData_folder + "Settings.xml"
        
        if os.path.isfile(self._xml_settings_file):
            self.settings.parse_xml_file(self._xml_settings_file)
            
            dict_default_paths = self.settings.get_launcher_paths(self._default_paths)        
            
            node_list = self.settings.get_node_list()
            dict_maya_plugins = self.settings.get_plugins()
            dict_node_graph = self.settings.get_node_graph()
            
            logging.log(logging.INFO, "Loaded last Settings")                    
            return dict_default_paths, node_list, dict_maya_plugins, dict_node_graph
        logging.log(logging.INFO, "Loaded empty Settings")
        return None
   
    
    def _del_(self):
        pass
