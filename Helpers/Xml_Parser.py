'''
Created on 27.12.2012

@author: danielgoertz
'''
import logging
from PyQt4 import QtCore

from Viewer.Node import Node as Node

import xml.etree.ElementTree as ET
import Helpers.Multi_Platform as multi_platform
from xml.etree.ElementTree import ElementTree

print(multi_platform.get_current_platform())

class Xml_Parser(object):
    
    def __init__(self):
        self._tree = ""
        self._root = ""
        
    def parse_xml_file(self, path):
        try:
            self._tree = ET.parse(path)
            logging.log(logging.INFO, "Parsed XML file: " + path)
            self._root = self._tree.getroot()
        except Exception, e:
            logging.log(logging.ERROR, e.message)
    
    def get_support_programs(self):
        name_list = []
        for child in self._root:
            for sub_child in child:
                name_list.append(sub_child.attrib["name"])
        return name_list 
    
    def get_support_version_by_name(self, program_name):
        version_list = []
        for child in self._root:
            for sub_child in child:
                if sub_child.attrib["name"] == program_name:
                    for version in sub_child.iter("version"):
                        version_list.append(version.text)
        return version_list
    
    def get_script_path_by_name(self, program_name, platform):
        script_path = ""
        for child in self._root:
            for sub_child in child:
                if sub_child.attrib["name"] == program_name:
                    for version in sub_child.iter("platform"):
                        if version.attrib["name"] == platform:
                            script_path = version.text
        return script_path
    
    
    def get_launcher_paths(self, dict_default_paths):
        for child in self._root:
            if child.attrib["type"] == "Path":
                for sub_child in child:
                    print sub_child.attrib["name"]
                    for key in dict_default_paths:
                        if sub_child.attrib["name"] == key:
                            dict_default_paths[key] = sub_child.text
        return dict_default_paths
    
    def set_launcher_paths(self, xml_file, dict_launcher_paths):
        for child in self._root:
            if child.attrib["type"] == "Path":
                for sub_child in child:
                    for key in dict_launcher_paths:
                        if sub_child.attrib["name"] == key:
                            sub_child.text = dict_launcher_paths[key]
                            logging.info("Saved Launcher Path: " + dict_launcher_paths[key])
        self._tree.write(xml_file)  
    
    def set_plugins(self, xml_file, dict_maya_plugins):
        for child in self._root:
            if child.attrib["type"] == "Plugin":
                for sub_child in child:
                    for key in dict_maya_plugins:
                        if sub_child.attrib["name"] == key:
                            sub_child.text = dict_maya_plugins[key]
                            logging.info("Saved installed Plugins: " + dict_maya_plugins[key])
        self._tree.write(xml_file)  
    
    def get_plugins(self):
        
        dict_maya_plugins = {"GeoCache": "",
                             "Camera": "",
                             "QC": "",
                             "FurRIB": "",
                             "Batch": ""
                             }
        
        for child in self._root:
            if child.attrib["type"] == "Plugin":
                for sub_child in child:
                    for key in dict_maya_plugins:
                        if sub_child.attrib["name"] == key:
                            dict_maya_plugins[key] = sub_child.text
                            #logging.info("Loaded installed Plugins: " + dict_maya_plugins[key])
        return dict_maya_plugins
    
    def set_node_list(self, xml_file, node_list):
        # Remove all old Node-Elements
        for child in self._root:
            if child.attrib["type"] == "NodeList":
                for node in child.findall('node'):
                        child.remove(node)
                self._tree.write(xml_file)
        
        if node_list.count() > 0:
            for index in xrange(node_list.count()):

                node_path   =   str(node_list.item(index).data(QtCore.Qt.UserRole).toPyObject()[0])
                node_name   =   str(node_list.item(index).data(QtCore.Qt.UserRole).toPyObject()[1])
                node_color  =   str(node_list.item(index).data(QtCore.Qt.UserRole).toPyObject()[2].name())
                node_root   =   node_list.item(index).data(QtCore.Qt.UserRole).toPyObject()[3]
                    
                logging.log(logging.INFO, "Saved Node from Node List: " + node_name +", "+ node_path + ", " + node_color + ", " + str(node_root))
                
                # Add now the new Node-Elements
                for child in self._root:
                    if child.attrib["type"] == "NodeList":                                
                        temp = ET.SubElement(child, "node", {"type":"Node"})
                        temp_name = ET.SubElement(temp, "name")
                        temp_name.text = node_name
                        temp_path = ET.SubElement(temp, "path")
                        temp_path.text = node_path
                        temp_color = ET.SubElement(temp, "color")
                        temp_color.text = node_color
                        temp_root = ET.SubElement(temp, "root")
                        temp_root.text = node_root                        
                self._tree.write(xml_file)
                            
    def save_node_graph(self, xml_file, node_graph):        
        root = ET.Element("root")
        nodeGraph = ET.SubElement(root, "nodeGraph")
        nodeGraph.set("type","NodeGraph")
        
        for item in node_graph.items():
            if type(item) == Node:                               
                
                node = ET.SubElement(nodeGraph, "node")
                node.set("type","Node")
                
                node_name = ET.SubElement(node, "name")
                node_name.text = str(item.getName())
                
                is_root = ET.SubElement(node, "root")
                is_root.text = str(item.is_root())
                
                node_path = ET.SubElement(node, "path")
                node_path.text = str(item.getPath())
                
                node_color = ET.SubElement(node, "color")
                node_color.text = str(item.getColor().name())
                
                node_pos = ET.SubElement(node, "pos")
                node_pos.text = item.get_pos()
                
                logging.log(logging.INFO, "Saved Node : " + node_name +", "+ node_path + ", " + node_color + ", " + str(item.is_root()))
                                        
                for edge in item.edges():
                    if edge.destNode().getName() == item.getName():
                        temp_edge = ET.SubElement(node, "edge", {"type":"input"})
                        
                        temp_edge_name = ET.SubElement(temp_edge, "destNode")
                        temp_edge_name.text = str(edge.sourceNode().getName())
                                                                        
                        temp_edge_src_point = ET.SubElement(temp_edge, "sourcePoint")
                        temp_edge_src_point.text = edge.get_source_point() 
                        
                        temp_edge_dest_point = ET.SubElement(temp_edge, "destPoint")
                        temp_edge_dest_point.text = edge.get_dest_point()
                    else:
                        temp_edge = ET.SubElement(node, "edge", {"type":"output"})
                        
                        temp_edge_name = ET.SubElement(temp_edge, "destNode")
                        temp_edge_name.text = str(edge.destNode().getName())
                                                                        
                        temp_edge_src_point = ET.SubElement(temp_edge, "sourcePoint")
                        temp_edge_src_point.text = edge.get_source_point()
                        
                        temp_edge_dest_point = ET.SubElement(temp_edge, "destPoint")
                        temp_edge_dest_point.text = edge.get_dest_point()         
        try:
            xml_tree = ElementTree(root)
            xml_tree.write(xml_file)
            logging.log(logging.INFO, "Write XML file: " + xml_file)
        except Exception, e:            
            logging.log(logging.ERROR, e.message)
    
    def set_node_graph(self, xml_file, node_graph):
        
        for child in self._root:
            if child.attrib["type"] == "NodeGraph":
                for node in child.findall('node'):
                        child.remove(node)
                self._tree.write(xml_file)
                
        for item in node_graph.items():
            if type(item) == Node:
                for child in self._root:
                    if child.attrib["type"] == "NodeGraph":                                
                        temp = ET.SubElement(child, "node", {"type":"Node"})
                        
                        temp_name = ET.SubElement(temp, "name")
                        temp_name.text = str(item.getName())
                        
                        temp_path = ET.SubElement(temp, "path")
                        temp_path.text = str(item.getPath())
                        
                        is_root = ET.SubElement(temp, "root")
                        is_root.text = item.is_root()
                        
                        temp_color = ET.SubElement(temp, "color")
                        temp_color.text = str(item.getColor().name())
                        
                        temp_pos = ET.SubElement(temp, "pos")
                        temp_pos.text = item.get_pos()
                        
                        logging.log(logging.INFO, "Saved Node : " + temp_name.text +", "+ temp_path.text + ", " + temp_color.text + ", " + str(item.is_root()))
                                        
                for edge in item.edges():
                    if edge.destNode().getName() == item.getName():
                        temp_edge = ET.SubElement(temp, "edge", {"type":"input"})
                        
                        temp_edge_name = ET.SubElement(temp_edge, "destNode")
                        temp_edge_name.text = str(edge.sourceNode().getName())
                                                                        
                        temp_edge_src_point = ET.SubElement(temp_edge, "sourcePoint")
                        temp_edge_src_point.text = edge.get_source_point() 
                        
                        temp_edge_dest_point = ET.SubElement(temp_edge, "destPoint")
                        temp_edge_dest_point.text = edge.get_dest_point()
                    else:
                        temp_edge = ET.SubElement(temp, "edge", {"type":"output"})
                        
                        temp_edge_name = ET.SubElement(temp_edge, "destNode")
                        temp_edge_name.text = str(edge.destNode().getName())
                                                                        
                        temp_edge_src_point = ET.SubElement(temp_edge, "sourcePoint")
                        temp_edge_src_point.text = edge.get_source_point()
                        
                        temp_edge_dest_point = ET.SubElement(temp_edge, "destPoint")
                        temp_edge_dest_point.text = edge.get_dest_point()                        
        try:                
            self._tree.write(xml_file)
            logging.log(logging.INFO, "Write XML file: " + xml_file)
        except Exception, e:            
            logging.log(logging.ERROR, e.message)    
    
    def get_node_list(self):
        result_node_list = []
        counter = 0
        
        for child in self._root:
            if child.attrib["type"] == "NodeList":
                for node_list in child:
                    if node_list.attrib["type"] == "Node":
                        result_node_list.append([])
                        for sub_element in node_list:
                            result_node_list[counter].append(sub_element.tag)
                            result_node_list[counter].append(sub_element.text)
                        counter += 1 
        return result_node_list

    
    def get_node_graph(self):
        result_node_graph = []
        
        for child in self._root:
            if child.attrib["type"] == "NodeGraph":
                for node_list in child:
                    
                    node_dict = {"name":"", "path": "", "color": "", "pos": "", "root": ""}
                    edge_dict = {"input": [], "output": []}
                    
                    if node_list.attrib["type"] == "Node":
                        input_list = []
                        output_list = []
                        
                        for sub_element in node_list:
                            for key in node_dict:
                                if sub_element.tag == key:
                                    node_dict[key] = sub_element.text                                 
                                    
                            if sub_element.tag == "edge":
                                edge = {"destNode": "", "sourcePoint": "", "destPoint": ""}
                                                                    
                                if sub_element.attrib["type"] == "input":
                                    for edge_attr in sub_element:
                                        if edge_attr.tag == "destNode":
                                            edge[edge_attr.tag] = edge_attr.text
                                            
                                        elif edge_attr.tag == "sourcePoint":
                                            edge[edge_attr.tag] = edge_attr.text
                                            
                                        elif edge_attr.tag == "destPoint":
                                            edge[edge_attr.tag] = edge_attr.text                                            
                                    input_list.append(edge)
                                else:
                                    for edge_attr in sub_element:
                                        if edge_attr.tag == "destNode":
                                            edge[edge_attr.tag] = edge_attr.text
                                            
                                        elif edge_attr.tag == "sourcePoint":
                                            edge[edge_attr.tag] = edge_attr.text
                                            
                                        elif edge_attr.tag == "destPoint":
                                            edge[edge_attr.tag] = edge_attr.text                                            
                                    output_list.append(edge)
                                        
                        edge_dict["input"] = input_list
                        edge_dict["output"] = output_list                                                 
                        result_node_graph.append([node_dict,edge_dict])
                        
        return result_node_graph
    
    def get_regEx(self):
        pass