@ECHO OFF
CLS
REM -----------------------------------------------------------------
REM Description: Shell Script is calling MEL script to start the Pipeline 
REM in MayaBatch 2011 x86
REM Author: Daniel Goertz - Exozet Effects
REM Version: v0.2
REM Date: 14.07.2011
REM -----------------------------------------------------------------

"C:\Program Files\Autodesk\Maya2011\bin\mayabatch.exe" -command "startPipeline"

ECHO END
REM PAUSE