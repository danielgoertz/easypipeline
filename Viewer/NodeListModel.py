'''
Created on 19.01.2013
NOT USED AT THE MOMENT!
@author: danielgoertz
'''
from PyQt4 import QtCore

class NodeListModel(QtCore.QAbstractListModel):
    
    def __init__(self, datain, parent=None, *args): 
        """ datain: a list where each item is a row
        """
        QtCore.QAbstractListModel.__init__(self, parent, *args) 
        self.listdata = datain
 
    def rowCount(self, parent=QtCore.QModelIndex()): 
        return len(self.listdata) 
 
    def data(self, index, role): 
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return QtCore.QVariant(self.listdata[index.row()])
        else: 
            return QtCore.QVariant()

    def insertRows(self):
        pass
    
    def removeRows(self):
        pass