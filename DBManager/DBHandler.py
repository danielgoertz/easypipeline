'''
Created on 11.02.2013

@author: danielgoertz
'''
import logging
import pymongo
from DBManager import User_Data 

user = User_Data.User_Data("Daniel Goertz", "Admin", "TD", "", "Mac", "")

class DBHandler(object):
    def __init__(self, host="localhost", port=27017):
        try:
            self.connection = pymongo.MongoClient(host, port)
            db = self.connection['test-database']
            self.user_collection = db['user_collection']
            self.meta_data_collection = db['meta_data_collection']
            self.node_type_collection = db['node_type_collection']
        except Exception, e:
            logging.log(logging.ERROR, e.message)
            print "could not connect to the DataBase with parameters:localhost:27017"
    
    def test_connection(self):
        print "test"
        
    def init_db(self):
        #TOOD: Create all necessarily db collections 
        pass
        
    
    def insert_into_user_collection(self, doc_object):
        self.user_collection.insert(doc_object)
        
    
    
#handler = Handler()
#handler.insert_into_user_collection(user.get_user_data_dict())