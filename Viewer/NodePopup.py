'''
Created on 20.01.2013

@author: danielgoertz
'''

from PyQt4 import QtGui, QtCore
import Helpers.Multi_Platform as Multi_platform

Platform = Multi_platform.get_current_platform()

class NodePopup(QtGui.QWidget):
    
    def __init__(self, listWidget_node_list):
        super(NodePopup, self).__init__()
        self.initUI()
        self.lv = listWidget_node_list
        self.color = None
        
    def initUI(self):
                
        self.labelPath = QtGui.QLabel(self)
        self.labelPath.move(10, 20)
        self.labelPath.setText("Path:")
        self.pathInput = QtGui.QLineEdit(self)
        self.pathInput.setFixedWidth(300)
        self.pathInput.move(60, 20)
        self.btnBrowsePath = QtGui.QPushButton('Browse Path', self)
        self.btnBrowsePath.move(370, 15)
        self.btnBrowsePath.clicked.connect(self.browsePath)
             
        self.labelName = QtGui.QLabel(self)
        self.labelName.move(10, 60)
        self.labelName.setText("Name:") 
        self.nameInput = QtGui.QLineEdit(self)
        self.nameInput.setFixedWidth(300)
        self.nameInput.move(60, 60)
        self.nameInput.setBackgroundRole(QtGui.QPalette.Dark)
  
        self.labelColor = QtGui.QLabel(self)
        self.labelColor.move(10, 105)
        self.labelColor.setText("Color:") 
        self.btnselectedColorDialog = QtGui.QPushButton('Select Color', self)
        self.btnselectedColorDialog.move(370, 100)
        self.btnselectedColorDialog.clicked.connect(self.showColorDialog)
        self.colorReviewer = QtGui.QLabel(self)
        self.qPixMap = QtGui.QPixmap(300,20)
        self.qPixMap.fill(QtCore.Qt.gray)
        self.colorReviewer.setPixmap(self.qPixMap) 
        self.colorReviewer.move(60, 105)
        
        self.labelRoot = QtGui.QLabel(self)
        self.labelRoot.move(10, 145)
        self.labelRoot.setText("Root:")
        self.checkBoxRoot = QtGui.QCheckBox('', self)
        self.checkBoxRoot.move(60, 145)        
              
        self.btnAdd = QtGui.QPushButton('Add', self)
        self.btnAdd.move(2, 170)
        self.btnAdd.clicked.connect(self.saveNodeData)
        
        self.btnClose = QtGui.QPushButton('Close', self)
        self.btnClose.move(280, 170)
        self.btnClose.clicked.connect(self.closePopUp)

        self.setWindowTitle('New Directory-Node')
        self.show()
        
    def showColorDialog(self):       
        selectedColor = QtGui.QColorDialog.getColor()
        if selectedColor.isValid():
            self.qPixMap.fill(selectedColor)
            self.color = selectedColor
            self.colorReviewer.setPixmap(self.qPixMap) 
            self.colorReviewer.repaint()
            
    def saveNodeData(self):
        if self.checkBoxRoot.checkState() == QtCore.Qt.Checked:
            is_root = "True"
        else:
            is_root = "False"
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r"/Users/danielgoertz/Dropbox/workspace/AssetManager/Doc/Img/Setup_Screen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        item = QtGui.QListWidgetItem(self.nameInput.text())
        item.setIcon(icon)
        item.setData(QtCore.Qt.UserRole, (self.pathInput.text(),self.nameInput.text(), self.color, is_root))
        self.lv.addItem(item)
        
    def browsePath(self):
        if Platform == "macosx":
            path = QtGui.QFileDialog.getExistingDirectory(self, self.tr('Select output directory'), '/')
        if Platform == "windows":
            path = QtGui.QFileDialog.getExistingDirectory(self, self.tr('Select output directory'), 'C:\\')            
        self.pathInput.setText(path)
    
    def closePopUp(self):
        self.close()
            
