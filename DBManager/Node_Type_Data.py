'''
Created on 11.02.2013

@author: danielgoertz
'''
class Node_Type_Data(object):

    def __init__(self, node, user_name):
        self.node_name = node.getName()
        self.node_color = node.getColor()
        self.node_path = node.getPath()
        self.user_name = user_name
    
        self.node_type_dict = {"user_name": self.user_name, "node_name": self.node_name, "node_color": self.node_color, "node_path": self.node_path}
    
    def get_node_type_dict(self):
        return self.node_type_dict    

    def push_node_type_data(self):
        pass
        
    def pull_node_type_data(self):
        pass
    
    def __del__(self):
        pass