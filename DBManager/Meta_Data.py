'''
Created on 11.02.2013

@author: danielgoertz
'''

class Meta_Data(object):

    def __init__(self, name_of_artist, comment, file_type, screenshot):
        self.name_of_artist = name_of_artist
        self.modify_date = None 
        self.comment = comment
        self.file_type = file_type
        self.screenshot = screenshot
        self.version = None
    
        self.meta_data_dict = {"name of artist": self.name_of_artist, "modify date": self.modify_date, "comment": self.comment, "file type": self.file_type, "screenshot": self.screenshot, "version": self.version}
    
    def get_meta_data_dict(self):
        return self.meta_data_dict    

    def push_meta_data(self):
        pass
        
    def pull_meta_data(self):
        pass
    
    def __del__(self):
        pass