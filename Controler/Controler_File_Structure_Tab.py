'''
Created on 31.12.2012

@author: danielgoertz
'''
import os.path
import logging
from PyQt4 import QtCore

from Helpers import Xml_Parser
from Viewer.Node import Node as Node

class Controler_File_Structure_Tab(object):
    
    def __init__(self):
        self._node_list = ""
        self._xml_parser = Xml_Parser.Xml_Parser()
    
    def __del__(self):
        pass
    
    def update_node_list(self, listWidget_node_list):
        self._node_list = listWidget_node_list
    
    def remove_item(self, listWidget_node_list):
        try: 
            for item in listWidget_node_list.selectedItems():
                listWidget_node_list.takeItem(listWidget_node_list.row(item))
        except Exception, e:
            logging.log(logging.ERROR, e.message)
            
    def remove_edge(self, qGraphicsScene, edge):
        try:
            qGraphicsScene.removeItem(edge)
        except Exception, e:
            logging.log(logging.ERROR, e.message)
        
    def clean_qGraphicsScene(self, qGraphicsScene):
        for item in qGraphicsScene.items():
            qGraphicsScene.removeItem(item)            
            
    def scale_view(self, view, scale_factor):
        factor = view.matrix().scale(scale_factor, scale_factor).mapRect(QtCore.QRectF(0, 0, 1, 1)).width()
        if factor < 0.07 or factor > 100:
            return
        view.scale(scale_factor, scale_factor)
        
    def normalize_view(self, view, scale_factor):
        if scale_factor > 0.0:
            scale_factor = 1 / abs(scale_factor)
        else:
            scale_factor = abs(scale_factor)      
        view.scale(scale_factor, scale_factor)       
    
    def save_node_graph(self, xml_file, node_graph):
        self._xml_parser.save_node_graph(xml_file, node_graph)
        
    def load_node_graph(self, xml_file):
        self._xml_parser.parse_xml_file(xml_file)        
        return self._xml_parser.get_node_graph()
        
    def undirected_cycle_test(self, node_graph):
        # if return TRUE --> CYCLE is in the tree-structure !! 
        # if return FLASE --> NO CYCLE :)
        
        node_list = {}
        visited = {}
        has_entry_point = ""
        
        for item in node_graph.items():
            if type(item) == Node:
                if (item.is_root() == "True"):
                    has_entry_point = item.getName()
                visited.update({item.getName() : False})
                edges = []
                for edge in item.edges():
                    if edge.destNode().getName() == item.getName():
                        edges.append(edge.sourceNode().getName())
                    else:
                        edges.append(edge.destNode().getName())
                node_list.update({item.getName(): edges})
    
        def visit(node, from_node):
            if visited.has_key(node):
                if not visited[node]:
                    visited[node] = True 
                        
                    print node
                    print node_list[node]
                    for neighbor in node_list[node]:
                        if neighbor == from_node:
                            continue
                        if visit(neighbor, node):
                            return True
                    return False
                else:
                    return True

        if has_entry_point != "":
            startnode = has_entry_point
            return visit(startnode, startnode)
        else:
            return "No Node as Root was defined!"
        
        
    def create_data_structure(self, node_graph):
        """
        """
        if (self.undirected_cycle_test(node_graph) == False):
            # DFS method
            node_list = {}
            visited = {}
            has_entry_point = ""
            root_path = ""
            
            for item in node_graph.items():
                if type(item) == Node:
                    if (item.is_root() == "True"):
                        has_entry_point = item.getName()
                        root_path = item.getPath()                  
                    visited.update({item.getName() : False})
                    edges = []
                    for edge in item.edges():
                        if edge.destNode().getName() == item.getName():
                            edges.append(edge.sourceNode().getName())
                        else:
                            edges.append(edge.destNode().getName())
                    node_list.update({item.getName(): edges})
    
            def visit(node, root_path):
                if visited.has_key(node):
                    if not visited[node]:
                        visited[node] = True                    
                        for item in node_graph.items():
                            if type(item) == Node:
                                if (item.getName() == node and node != has_entry_point):
                                    root_path = root_path + item.getPath()
                                    print root_path
                                    if not os.path.exists(root_path):
                                        try: 
                                            os.makedirs(root_path)
                                            logging.log(logging.INFO, "Created folder structure: " + root_path)
                                        except Exception, e:
                                            logging.log(logging.ERROR, e.message)
                        for neighbor in node_list[node]:
                            visit(neighbor, root_path)        
    
            if has_entry_point != "":
                startnode = has_entry_point
                return visit(startnode, root_path)
            else:
                return "No Node as Entry-Point was defined or found!"
        else:
            return "The Node Graph consists a Loop! Please check your Node Graph"
                
